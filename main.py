import requests
import json
import logging
import os

logging.basicConfig(level=logging.INFO)

# URL = "https://aydym.com/api/v1/song?offset=0&max=50&sort=albumOrder&order=asc&albumId=46"
URL = input("Enter url: ")
folder = input("Enter folder name: ") or "music"
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

page = requests.get(URL, headers=headers)

if page.status_code != 200:
    logging.fatal("Invalid status code")
else:
    logging.info("Valid status code 200")

os.mkdir(folder)

api_resp = json.loads(page.text)

for ind, music in enumerate(api_resp['data']):
    name = f"{ind}.{music['name']}-{music['artist']}"
    logging.info(f"request for {name}")
    music_request = requests.get(music['url'], headers=headers)
    with open(folder + "/" + name + ".mp3", "wb") as fout:
        fout.write(music_request.content)
    logging.info(f"request for {name} - DONE")